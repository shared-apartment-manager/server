<!-- Prevent duplicates! Search in the already existing issues for your topic. -->

# Feature Request

<!--
    Please fill all points in the following categories to clearify your request.
    Consider adding related issues/topics aswell.
-->

## Proposal

<!-- Use this section to explain your idea roughly -->

## Problem Discription

<!--
    What is the problem you want to be solved? Questions you should answer:
        - What should we know, to understand the issue?
        - Where is the problem located?
        - (Optional) Why should this Problem be fixed?
 -->

## Further details

<!-- Other additional useful information can be:
    - What does success look like? (and how can we measure that?)
    - related issues/topics
    - use cases, benefits, other details that will help to understand the problem better. -->

### Realization

<!--
    (Optional) Describe how the problem can be solved:
        - Which part of the project would be affected?
        - What is your proposal to fix the problem(what would you do to fix)?
        - Which solutions may already exist?
        - Why is your solution better than existing solutions?
-->

## Links / References

<!-- Please select labels etc. wich categorizes your issue the best. -->

/label ~feature request

<!-- Inspirations for this template from the GitLab project. -->
